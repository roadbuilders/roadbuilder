﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{
    public Rigidbody rb;
    public float speed;

    // Update is called once per frame
    void Update()
    {
        rb.AddForce(speed, 0, 0);
        
    }
}
